<?php
/**
 * Token ownership tests for EthPress
 *
 * @since 0.1.0
 *
 * @package Ethpress_Tokens
 */

namespace losnappas\Ethpress_Tokens;

defined( 'ABSPATH' ) || die;

use losnappas\Ethpress_Tokens\Etherscan;
// Note that this file needs to be required by the time we're here.
// Composer cannot find it, since different vendor folders.
// Well, it always is.
use losnappas\Ethpress\Address;

/**
 * Runs the show.
 *
 * Checks how many tokens a user has, then, if they have none, prevents login.
 *
 * @since 0.1.0
 */
class Plugin {

	/**
	 * Attaches plugin's hooks.
	 *
	 * @since 0.1.0
	 */
	public static function attach_hooks() {
		add_action(
			'ethpress_login',
			array( __CLASS__, 'ethpress_login' )
		);

		if ( is_admin() ) {
			self::attach_admin_hooks();
		}
	}

	/**
	 * Attaches admin hooks.
	 *
	 * @since 0.1.0
	 */
	public static function attach_admin_hooks() {
		add_action(
			'admin_menu',
			array( ETHPRESS_TOKENS_NS . '\Admin\Options', 'admin_menu' )
		);
		add_action(
			'admin_init',
			array( ETHPRESS_TOKENS_NS . '\Admin\Options', 'admin_init' )
		);
		$plugin = plugin_basename( ETHPRESS_TOKENS_FILE );
		add_filter( "plugin_action_links_$plugin", array( ETHPRESS_TOKENS_NS . '\Admin\Options', 'plugin_action_links' ) );
	}

	/**
	 * Attached to ethpress_login hook.
	 *
	 * @since 0.1.0
	 *
	 * @param (WP_User|WP_Error) $user User or error, from the hook.
	 */
	public static function ethpress_login( $user ) {
		if ( is_wp_error( $user ) ) {
			// Failed login. Bail.
			return;
		}

		$address = Address::find_by_user( $user->ID );

		$tokens = self::check_tokens( $address );

		if ( is_wp_error( $tokens ) ) {
			$error_message = $tokens->get_error_message();
		}
		if ( '0' === $tokens ) {
			$options         = get_option(
				'ethpress_tokens',
				array(
					/* translators: This is the default value to "Not enough __tokens__." */
					'token_clearname' => __( 'tokens', 'ethpress_tokens' ),
				)
			);
			$token_clearname = $options['token_clearname'];
			$error_message   = esc_html(
				sprintf(
					/* translators: token name. */
					__( 'You need to own %1$s to log in.', 'ethpress_tokens' ),
					$token_clearname
				)
			);
		}

		// Block user if error, or if 0 tokens.
		if ( $error_message ) {
			$user = $address->get_user();
			if ( $user ) {
				clean_user_cache( $user->ID );
			}
			wp_clear_auth_cookie();
			wp_send_json_error(
				$error_message
			);
		}
	}

	/**
	 * Runs on hook.
	 *
	 * @since 0.1.0
	 *
	 * @param object $address EthPress Address object.
	 * @return (string|WP_Error) Amount of tokens or WP_Error.
	 */
	public static function check_tokens( $address ) {
		$options          = get_option(
			'ethpress_tokens',
			array()
		);
		$contract_address = $options['contract_address'];

		// No token specified in settings. Bail.
		if ( empty( $contract_address ) ) {
			return;
		}

		if ( is_wp_error( $address ) ) {
			$token_balance = false;
		} else {
			$coinbase      = $address->get_coinbase();
			$token_balance = Etherscan::get_token_ownership( $coinbase, $contract_address );
		}
		if ( ! is_string( $token_balance ) || ! is_numeric( $token_balance ) ) {
			// Most likely "RATE LIMITED" by Etherscan.
			// To fix, use an API key!
			return new \WP_Error( 'ethpress_tokens', esc_html__( 'Unexpected error, try again soon.', 'ethpress_tokens' ) );
		}
		return $token_balance;
	}
}
